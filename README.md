###### start app
- docker-compose up

##### routes:
- GET http://localhost:3000/user
- GET http://localhost:3000/user/:id
- POST http://localhost:3000/user (body {name, age, sex })
- PUT http://localhost:3000/user/:id (body {name, age, sex })
- DELETE http://localhost:3000/user/:id
