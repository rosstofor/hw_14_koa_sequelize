const {
  getUsersService,
  getUserService,
  updateUserService,
  deleteUserService,
  createUserService,
  checkUserService,
} = require("../services/user");

const getUsers = async (ctx) => {
  try {
    const data = await getUsersService();
    ctx.response.body = { success: true, data };
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};
const getUser = async (ctx) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const candidate = await checkUserService(id);
    if (!candidate) {
      return (
        (ctx.response.body = {
          data: { message: `User with id: ${id} not exist` },
        }),
        (ctx.response.status = 400)
      );
    }

    const data = await getUserService(id);
    ctx.response.body = { success: true, data };
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};
const createUser = async (ctx) => {
  try {
    const user = {
      name: ctx.request.body.name,
      age: ctx.request.body.age,
      sex: ctx.request.body.sex,
    };
    const data = await createUserService(user);
    ctx.response.status = 201;
    ctx.response.body = { success: true, data };
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};
const updateUser = async (ctx) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const candidate = await checkUserService(id);
    if (!candidate) {
      return (
        (ctx.response.body = {
          data: { message: `User with id: ${id} not exist` },
        }),
        (ctx.response.status = 400)
      );
    }

    const user = {
      name: ctx.request.body.name,
      age: ctx.request.body.age,
      sex: ctx.request.body.sex,
    };
    await updateUserService(user, id);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: { message: "Updated" } };
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};
const deleteUser = async (ctx) => {
  try {
    const id = ctx.request.url.split("/").pop();

    const candidate = await checkUserService(id);
    if (!candidate) {
      return (
        (ctx.response.body = {
          data: { message: `User with id: ${id} not exist` },
        }),
        (ctx.response.status = 400)
      );
    }

    await deleteUserService(id);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: { message: "Deleted" } };
  } catch (error) {
    ctx.response.body = { success: false, error };
    ctx.response.status = 400;
  }
};

module.exports = {
  getUser,
  getUsers,
  updateUser,
  createUser,
  deleteUser,
};
