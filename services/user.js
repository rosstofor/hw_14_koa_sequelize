const { sequelize, User } = require("../db/models");

const getUsersService = async () => {
  try {
    const data = await User.findAll({ order: [["id", "ASC"]] });
    return data;
  } catch (err) {
    return err;
  }
};
const getUserService = async (id) => {
  try {
    const data = await User.findOne({ where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};
const createUserService = async (user) => {
  try {
    const data = await User.create({ ...user });
    return data;
  } catch (err) {
    return err;
  }
};
const updateUserService = async (user, id) => {
  try {
    const data = await User.update({ ...user }, { where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};
const deleteUserService = async (id) => {
  try {
    const data = await User.destroy({ where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};

const checkUserService = async (id) => {
  try {
    const data = await User.findOne({ where: { id } });
    return data;
  } catch (err) {
    return err;
  }
};

module.exports = {
  getUsersService,
  getUserService,
  createUserService,
  deleteUserService,
  updateUserService,
  checkUserService,
};
