const Koa = require("koa");
const db = require("./db/models");
const bodyParser = require("koa-bodyparser");
const userRouter = require("./routes/user");
require("dotenv").config();

const app = new Koa();
console.log(process.env.NODE_ENV);
app.use(bodyParser());
app.use(userRouter.routes());

const start = async () => {
  try {
    await db.sequelize
      .sync({ force: false })
      .then(console.log("db syncronized"));
    app.listen(process.env.SERVER_PORT, () => {
      console.log("server started");
    });
  } catch (error) {
    console.error(error);
  }
};

start();
